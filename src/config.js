export default {
    "main_menu": [
        {
            "title": "About me",
            "target_id": "about-me"
        },
        {
            "title": "Relationships",
            "target_id": "relationships"
        },
        {
            "title": "Requirements",
            "target_id": "requirements"
        },
        {
            "title": "Users",
            "target_id": "users"
        },
        {
            "title": "Sign Up",
            "target_id": "sign-up"
        },
    ],

    "banner": {
        "title": "Test assignment for Frontend Developer position",
        "image": {
            "xl": "/static/images/banner/bg-1-xl.jpg",
            "lg": "/static/images/banner/bg-1-lg.jpg",
            "md": "/static/images/banner/bg-1-md.jpg",
            "sm": "/static/images/banner/bg-1-sm.jpg",
            "xs": "/static/images/banner/bg-1-xs.jpg"
        },
        "description": "<p>We kindly remind you that your test assignment should be submitted as a link to github/bitbucket repository.</p><p>Please be patient, we consider and respond to every application that meets minimum requirements. We look forward to your submission. Good luck!</p>"
    },

    "quaint": {
        "title": "Let's get ac quainted",
        "image": "/static/images/man-mobile.svg",
        "sub_title": "I am cool frontend developer",
        "description": "<h3>I am cool frontend developer</h3><p>When real users have a slow experience on mobile, they're much less likely to find what they are looking for or purchase from you in the future. For many sites this equates to a huge missed opportunity, especially when more than half of visits are abandoned if a mobile page takes over 3 seconds to load.</p><p>Last week, Google Search and Ads teams announced two new speed initiatives to help improve user-experience on the web. </p>"
    },

    "relationships": {
        "title": "About my relationships with web-development",
        "list": [
            {
                "image": "/static/images/html.svg",
                "image_title": "html",
                "title": "I'm in love with HTML",
                "description": "<p>Hypertext Markup Language (HTML) is the standard markup language for creating web pages and web applications.</p>"
            },
            {
                "image": "/static/images/css.svg",
                "image_title": "css",
                "title": "CSS is my best friend",
                "description": "<p>Cascading Style Sheets (CSS) is a  style sheet language used for describing the presentation of a document written in a markup language like HTML.</p>"
            },
            {
                "image": "/static/images/javascript.svg",
                "image_title": "javascript",
                "title": "JavaScript is my passion",
                "description": "<p>JavaScript is a high-level, interpreted programming language. It is a language which is also characterized as dynamic, weakly typed, prototype-based and multi-paradigm.</p>"
            },
        ]
    },

    "requirements": {
        "title": "General requirements for the test task",
        "image": "/static/images/man-laptop-v1.svg",
        "bg": {
            "xl": "/static/images/requirements/bg-1-xl.jpg",
            "lg": "/static/images/requirements/bg-1-lg.jpg",
            "md": "/static/images/requirements/bg-1-md.jpg",
            "sm": "/static/images/requirements/bg-1-sm.jpg",
            "xs": "/static/images/requirements/bg-1-xs.jpg"
        },
        "description": "<p>Users want to find answers to their questions quickly and data shows that people really care about how quickly their pages load. The Search team announced speed would be a ranking signal for desktop searches in 2010 and as of this month (July 2018), page speed will be a ranking factor for mobile searches too.</p><p>If you're a developer working on a site, now is a good time to evaluate your performance using our speed tools. Think about how performance affects the user experience of your pages and consider measuring a variety of real-world user-centric performance metrics.</p><p>Are you shipping too much JavaScript? Too many images? Images and JavaScript are the most significant contributors to the page weight that affect page load time based on data from HTTP Archive and the Chrome User Experience Report - our public dataset for key UX metrics as experienced by Chrome users under real-world conditions.</p>"
    },

    "users": {
        "title": "Our cheerful users",
        "sub_title": "Attention! Sorting users by registration date"
    },

    "register": {
        "title": "Register to get a work",
        "sub_title": "Attention! After successful registration and alert, update the list of users in the block from the top"
    },

    "footer": {
        "menu": [
            {
                "title": "News",
                "href": "javascript:;"
            },
            {
                "title": "Overview",
                "href": "javascript:;"
            },
            {
                "title": "Tutorials",
                "href": "javascript:;"
            },
            {
                "title": "FAQ",
                "href": "javascript:;"
            },
            {
                "title": "Blog",
                "href": "javascript:;"
            },
            {
                "title": "Design",
                "href": "javascript:;"
            },
            {
                "title": "Resources",
                "href": "javascript:;"
            },
            {
                "title": "Terms",
                "href": "javascript:;"
            },

            {
                "title": "Partners",
                "href": "javascript:;"
            },
            {
                "title": "Code",
                "href": "javascript:;"
            },
            {
                "title": "Guides",
                "href": "javascript:;"
            },
            {
                "title": "Conditions",
                "href": "javascript:;"
            },

            {
                "title": "Shop",
                "href": "javascript:;"
            },
            {
                "title": "Collaborate",
                "href": "javascript:;"
            },
            {
                "title": "Examples",
                "href": "javascript:;"
            },
            {
                "title": "Help",
                "href": "javascript:;"
            },
        ],
        "contacts": [
            {
                "title": "work.of.future@gmail.com",
                "type": "mailto"
            },
            {
                "title": "+38 (050) 789 24 98",
                "type": "tel"
            },
            {
                "title": "+38 (095) 556 08 45",
            },
        ],
        "socials": [
            {
                "href": "javascript:;",
                "type": "fb"
            },
            {
                "href": "javascript:;",
                "type": "ln"
            },
            {
                "href": "javascript:;",
                "type": "in"
            },
            {
                "href": "javascript:;",
                "type": "tw"
            },
            {
                "href": "javascript:;",
                "type": "pin"
            },
        ],
        "copyright": "&copy; abz.agency specially for the test task",
    },
}