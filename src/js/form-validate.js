"use strict";

export default function (form, rules, options) {
    if (!form) return false;

    var self = this;
    self.form = form;
    self.rules = rules;
    self.fields = {};
    self.counter = 0;
    self.submitButton = form.querySelector("[type='submit']");
    self.options = {
        "checkSubmitButton": false
    };

    self.init = function () {
        self.initRules();
        self.setOptions();

        self.reset();
        self.events();
    };

    self.reset = function () {
        self.counter = 0;

        for (var key in self.fields) {
            if (self.fields[key] && self.fields[key]["valid"]) {
                self.fields[key]["valid"] = false;
            }
        }

        self.checkSubmitButton();
    };

    self.updateFieldByName = function (name, status) {
        var parentNode = self.fields[name].node.parentNode;

        parentNode.classList.remove("success");
        parentNode.classList.remove("error");

        if (status === "success") {
            if (!self.fields[name]["valid"]) self.counterIncrement();
            self.fields[name]["valid"] = true;
            parentNode.classList.add("success");
        } else {
            if (self.fields[name]["valid"]) self.counterDecrement();
            self.fields[name]["valid"] = false;
            parentNode.classList.add("error");
        }

        self.checkSubmitButton();
    };

    self.setOptions = function () {
        if (!options) return false;

        for (var key in options) {
            self.options[key] = options[key];
        }
    };

    self.initRules = function () {
        self.form.setAttribute("novalidate", true);

        for (var key in self.rules) {
            var node = self.form.querySelector("[name='" + key + "']");
            var rules = self.rules[key];

            if (node) {
                self.fields[key] = {};
                self.fields[key]["node"] = node;
                self.fields[key]["rules"] = rules;
                self.fields[key]["valid"] = false;

                (function (node, rules) {
                    node.addEventListener("keyup", function (e) {
                        self.checkRules(e.target, rules);
                    });
                })(node, rules);
            }
        }
    };

    self.checkRules = function (node, rules) {
        if (rules["required"]) self.required(node, rules["required"]);
        if (rules["email"]) self.email(node);
        if (rules["phone"]) self.phone(node);

        self.checkSubmitButton();
    };

    self.required = function (node, rules) {
        var name = node.getAttribute("name");
        var parentNode = node.parentNode;
        var minLength = rules["minLength"] || node.getAttribute("minlength") || 1;
        var maxLength = rules["maxLength"] || node.getAttribute("maxlength") || 10000;
        var val = node.value;
        var length = val.length;

        parentNode.classList.remove("success");
        parentNode.classList.remove("error");

        if (length >= minLength && length <= maxLength) {
            parentNode.classList.add("success");
            if (!self.fields[name]["valid"]) self.counterIncrement();
            self.fields[name]["valid"] = true;
        } else {
            parentNode.classList.add("error");
            if (self.fields[name]["valid"]) self.counterDecrement();
            self.fields[name]["valid"] = false;
        }
    };

    self.email = function (node) {
        var name = node.getAttribute("name");
        var parentNode = node.parentNode;
        var val = node.value;
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var test = re.test(String(val).toLowerCase());

        parentNode.classList.remove("success");
        parentNode.classList.remove("error");

        if (test) {
            parentNode.classList.add("success");
            if (!self.fields[name]["valid"]) self.counterIncrement();
            self.fields[name]["valid"] = true;
        } else {
            parentNode.classList.add("error");
            if (self.fields[name]["valid"]) self.counterDecrement();
            self.fields[name]["valid"] = false;
        }
    };

    self.phone = function (node) {
        var name = node.getAttribute("name");
        var parentNode = node.parentNode;
        var val = node.value;
        var re = new RegExp('^[-() 0-9]+$');
        var test = re.test(String(val).toLowerCase());

        parentNode.classList.remove("success");
        parentNode.classList.remove("error");

        if (test) {
            parentNode.classList.add("success");
            if (!self.fields[name]["valid"]) self.counterIncrement();
            self.fields[name]["valid"] = true;
        } else {
            parentNode.classList.add("error");
            if (self.fields[name]["valid"]) self.counterDecrement();
            self.fields[name]["valid"] = false;
        }
    };

    self.counterIncrement = function () {
        self.counter += 1;
    };

    self.counterDecrement = function () {
        self.counter -= 1;

        if (self.counter < 0) self.counter = 0;
    };

    self.checkForm = function () {
        for (var key in self.fields) {
            var node = self.fields[key]["node"];
            var rules = self.fields[key]["rules"];

            self.checkRules(node, rules);
        }

        if (Object.keys(self.fields).length === self.counter) {
            self.submitHandler();
        }
    };

    self.checkSubmitButton = function () {
        if (!self.options.checkSubmitButton) return false;

        if (Object.keys(self.fields).length === self.counter) {
            self.submitButton.disabled = false;
        } else {
            self.submitButton.disabled = true;
        }
    };

    self.submitHandler = function () {};
    
    self.events = function () {

        self.form.addEventListener("submit", function (e) {
            e.preventDefault();
            self.checkForm();
        });

    };

    self.init();

    return self;
};