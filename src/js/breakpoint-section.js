let BreakpointSection = function (section) {
    if(!section) return false;

    let self = this;

    self.section = section;
    self.imagesData = {};
    self.currentSize = "";
    self.config = {
        'sizes': {
            'xs': 1,
            'sm': 576,
            'md': 992,
            'lg': 1280,
            'xl': 1920
        }
    };

    self.init = function () {
        let images = self.section.getElementsByClassName('js-breakpoint-image');
        let counter = 0;

        for (let i = 0; i < images.length; i++) {
            let image = images[i];

            self.imagesData[counter] = {};
            self.imagesData[counter]['node'] = image;
            self.imagesData[counter]['tagName'] = image.tagName;
            self.imagesData[counter]['breakpoints'] = {};

            for (let key in self.config.sizes) {
                let src = image.getAttribute('data-' + key + '-src');
                if (src) self.imagesData[counter]['breakpoints'][key] = src;
            }

            counter += 1;
        }

        // console.log(self.imagesData);
    };

    self.update = function () {
        let size = '';

        for (let key in self.config.sizes) {
            if (self.section.offsetWidth >= self.config.sizes[key]) {
                size = key;
            }
        }

        if (size !== self.currentSize) {
            self.currentSize = size;
            self.render();
        }
    };

    self.render = function () {
        for (let key in self.imagesData) {
            let data = self.imagesData[key];
            let src = '';

            for (let breakpointKey in data.breakpoints) {
                if (breakpointKey === self.currentSize) src = data.breakpoints[breakpointKey];
            }

            if (!src) {
                let _keys = Object.keys(data.breakpoints);
                src = data.breakpoints[Object.keys(data.breakpoints)[_keys.length-1] ];
            }

            if (data.tagName === 'IMG') {
                data.node.src = src;
            } else {
                data.node.style.backgroundImage = 'url(' + src + ')';
            }
        }
    };

    self.init();
    self.update();
};

export default BreakpointSection;