export function detectMobile() {
    let useragents = ['android','astel','audiovox','blackberry','chtml','docomo','ericsson','hand','iphone ','ipod','2me','ava','j-phone','kddi','lg','midp','mini','minimo','mobi','mobile','mobileexplorer','mot-e','motorola','mot-v','netfront','nokia', 'palm','palmos','palmsource','pda','pdxgw','phone','plucker','portable','portalmmm','sagem','samsung','sanyo','sgh','sharp','sie-m','sie-s','smartphone','softbank','sprint','symbian','telit','tsm','vodafone','wap','windowsce','wml','xiino'];
    let agt = navigator.userAgent.toLowerCase();
    let isMobile = false;

    for( let i = 0; i < useragents.length; i+=1 ){
        if (agt.indexOf(useragents[i]) != -1) {
            isMobile = true;
            break;
        }
    }

    if (isMobile) {
        document.documentElement.classList.add("is-mobile");
    } else {
        document.documentElement.classList.add("is-desktop");
    }
};

export function checkMatches() {
    // проверяем поддержку
    if (!Element.prototype.matches) {

        // определяем свойство
        Element.prototype.matches = Element.prototype.matchesSelector ||
            Element.prototype.webkitMatchesSelector ||
            Element.prototype.mozMatchesSelector ||
            Element.prototype.msMatchesSelector;

    }
};

export function checkClosest() {
    // проверяем поддержку
    if (!Element.prototype.closest) {

        // реализуем
        Element.prototype.closest = function(css) {
            var node = this;

            while (node) {
                if (node.matches(css)) return node;
                else node = node.parentElement;
            }
            return null;
        };
    }
};

export function debounce(func, wait, immediate) {
    let timeout;

    return function() {
        let context = this, args = arguments;
        let later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

export function scrollToTarget(target, speed, callbacks, offset) {
    if (typeof speed === "undefined") speed = 1500;

    let scroll = function (y) {
        smoothScrollTo(0, y, speed, {
            "onComplete": function () {
                if (callbacks && callbacks.onComplete) callbacks.onComplete();
            }
        });
    };

    if (!target) {
        return true;
    }

    let node = null;

    if (typeof target === "string") {
        node = document.querySelector(target);
    } else {
        node = target;
    }

    if (!node) return false;

    let top =  (window.pageYOffset || document.documentElement.scrollTop) + node.getBoundingClientRect().top;
    let header = document.querySelector(".header");

    if (header) {
        top -= header.offsetHeight;
    }

    if (offset) {
        top += offset;
    }

    scroll(top);
};

export function smoothScrollTo(endX, endY, duration, callbacks) {
    let startX = window.scrollX || window.pageXOffset,
        startY = window.scrollY || window.pageYOffset,
        distanceX = endX - startX,
        distanceY = endY - startY,
        startTime = new Date().getTime();

    duration = typeof duration !== 'undefined' ? duration : 400;

    // Easing function
    let easeInOutQuart = function(time, from, distance, duration) {
        if ((time /= duration / 2) < 1) return distance / 2 * time * time * time * time + from;
        return -distance / 2 * ((time -= 2) * time * time * time - 2) + from;
    };

    let timer = window.setInterval(function() {
        let time = new Date().getTime() - startTime,
            newX = easeInOutQuart(time, startX, distanceX, duration),
            newY = easeInOutQuart(time, startY, distanceY, duration);

        if (time >= duration) {
            window.clearInterval(timer);
            if (callbacks && callbacks.onComplete) callbacks.onComplete();
        }
        window.scrollTo(newX, newY);
    }, 1000 / 60); // 60 fps
};




