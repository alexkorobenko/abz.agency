import Vue from "vue";
import App from "./app";
import store from "./store/index.js";
import {EventBus} from "./event-bus";
import {detectMobile} from "./js/functions";
import {debounce} from "./js/functions";
import "./sass/main.scss";

Vue.config.productionTip = false;

window["breakpointSections"] = [];

detectMobile();

new Vue({
    el: '#app',
    store: store,
    template: '<App/>',
    components: { App },
    mounted () {
        this.getToken();
        this.$store.dispatch("getProfile", 1);
        this.events();
    },

    methods: {
        getToken () {
            let self = this;
            let now = new Date();
            let delay = 40 * 1000; // 40m in ms
            let start = delay - (now.getMinutes() * 60 + now.getSeconds()) * 1000 + now.getMilliseconds();

            setTimeout(function doSomething() {
                self.$store.dispatch("getToken");
                setTimeout(doSomething, delay);
            }, start);
        },

        events () {
            let resizeEventName = (document.documentElement.classList.contains("is-mobile")) ? "orientationchange" : "resize";

            window.addEventListener(resizeEventName, debounce( () => {
                window["breakpointSections"].forEach((el) => {
                    el.update();
                });

                EventBus.$emit("root::resize");
            }, 300));
        }
    }
});