import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import _ from "lodash";
import config from "../config";

Vue.use(Vuex);

const apiBaseUrl = "https://frontend-test-assignment-api.abz.agency/api/v1";

const store = new Vuex.Store({

    state: {
        token: null,
        profile: {},
        usersData: {},
        users: [],
        positions: [],
        config: config
    },

    actions: {
        getToken ({commit}) {
            return new Promise((resolve, reject) => {
                axios.get(apiBaseUrl + "/token")
                    .then( (response) => {
                        if (response.data && response.data.success) {
                            commit("setToken", response.data.token);
                            resolve(response.data.token);
                        }
                    })
                    .catch( (error) => {
                        reject(error.response.data);
                    });
            });
        },

        getProfile ({commit}, id) {
            return new Promise((resolve, reject) => {
                axios.get(apiBaseUrl + "/users/" + id)
                    .then( (response) => {
                        if (response.data && response.data.success) {
                            commit("setProfile", response.data.user);
                            resolve(response.data.user);
                        }
                    })
                    .catch( (error) => {
                        reject(error.response.data);
                    });
            });
        },

        getUsersData ({commit}, url) {
            return new Promise((resolve, reject) => {
                axios.get(url || apiBaseUrl + "/users?page=1&count=6")
                    .then( (response) => {
                        if (response.data && response.data.success) {
                            commit("setUsersData", response.data);
                            resolve(response.data);
                        }
                    })
                    .catch( (error) => {
                        reject(error.response.data);
                    });
            });
        },

        setUsers ({commit}, {formData, token}) {
            return new Promise((resolve, reject) => {
                axios({
                    method: "POST",
                    url: apiBaseUrl + "/users",
                    headers: {
                        "Token": token
                    },
                    data: formData
                })
                .then( (response) => {
                    if (response.data && response.data.success) {
                        resolve(response.data);
                    }
                })
                .catch( (error) => {
                    reject(error.response.data);
                });
            });
        },

        getPositions ({commit}) {
            return new Promise((resolve, reject) => {
                axios.get(apiBaseUrl + "/positions")
                    .then( (response) => {
                        if (response.data && response.data.success) {
                            commit("setPositions", response.data.positions);
                            resolve(response.data.positions);
                        }
                    })
                    .catch( (error) => {
                        reject(error.response.data);
                    });
            });
        },
    },

    mutations: {
        setToken (state, token) {
            state.token = token;
        },

        setProfile (state, profile) {
            state.profile = profile;
        },

        setUsersData (state, data) {
            state.usersData = data;
            if (data.page === 1) state.users = [];
            state.users = [...state.users, ...data.users];
        },

        setPositions (state, data) {
            state.positions = data;
        },
    },

    getters: {
        token: (state) => {
            return state.token;
        },

        profile: (state) => {
            return state.profile;
        },

        usersData: (state) => {
            return state.usersData;
        },

        users: (state) => {
            return _.orderBy(state.users, "registration_timestamp", "desc");
        },

        positions: (state) => {
            return state.positions;
        },

        config: (state) => (fieldName) => {
            return state.config[fieldName] || state.config;
        }
    }

});

export default store;